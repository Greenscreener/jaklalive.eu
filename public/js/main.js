const apiKey = "AIzaSyCy1vIPr8ZrunX3ZnyMbBBGuFOASIie7tQ";

function toggleMenu() {
	document.querySelectorAll("#navbar-menu, #navbar-burger").forEach(e => {
		e.classList.toggle("is-active");
	})
}
document.addEventListener("DOMContentLoaded", () => {
	document.querySelectorAll("#navbar-menu, #navbar-burger").forEach(e => {
		e.addEventListener("click", () => {
			toggleMenu();
		})
	});
	fetch("https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=10&playlistId=PLYsyU3Xc_lxQOd1muTErptd2RZJUjaSXz&key=" + apiKey)
		.then(response => response.json()).then(json => {
			const upcoming = json.items.filter(e => e.snippet.description.indexOf("#upcoming") > -1);
			const live = json.items.filter(e => e.snippet.description.indexOf("#live") > -1);
			const previous = json.items.filter(e => e.snippet.description.indexOf("#upcoming") === -1 && e.snippet.description.indexOf("#live") === -1);

			if (live.length > 0) {
				document.getElementById("latestLabelTarget").innerText = "Právě probíhající přenos:";
				renderLatest(live[0]);
			} else if (upcoming.length > 0) {
				document.getElementById("latestLabelTarget").innerText = "Nejbližší naplánovaný přenos:";
				renderLatest(upcoming[0]);
			} else {
				document.getElementById("latestLabelTarget").innerText = "Poslední přenos:";
				renderLatest(previous[0]);
			}

			const previousVisible = previous.slice(0,6);
			const previousTarget = document.getElementById("previousTarget");
			previousVisible.forEach(e => {
				previousTarget.innerHTML+=`
					<div class="column is-4">
						<a href="https://youtube.com/watch?v=${e.snippet.resourceId.videoId}" class="card has-same-height">
							<div class="card-image">
								<figure class="image is-16by9">
									<img src="https://img.youtube.com/vi/${e.snippet.resourceId.videoId}/maxresdefault.jpg" alt="Thumbnail">
								</figure>
							</div>
							<div class="card-content">
								<div class="content">
									<h4 class="title is-size-4"> ${e.snippet.title} </h4>
									<p class="yt-description">${e.snippet.description}</p>
								</div>
							</div>
						</a>
					</div>
				`;
			});
		});
});
function renderLatest(video) {
	document.getElementById("latestTarget").innerHTML = `
		<a href="https://youtube.com/watch?v=${video.snippet.resourceId.videoId}">
			<div class="card">
				<div class="card-image">
					<figure class="image is-16by9">
						<img src="https://img.youtube.com/vi/${video.snippet.resourceId.videoId}/maxresdefault.jpg" alt="Thumbnail">
					</figure>
				</div>
				<div class="card-content">
					<div class="content">
						<h4 class="title is-size-4"> ${video.snippet.title} </h4>
						<p class="yt-description">${video.snippet.description}</p>
					</div>
				</div>
			</div>
		</a>
	`;
}
